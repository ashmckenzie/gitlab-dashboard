# GitLab Dashboard

This is a GitLab Dashboard that is designed to complement the workflow in which
GitLab team members work.

<img src="screenshot1.png" alt="screenshot" width="600"/>

## Requirements

- [Node.js 18.17.0+](https://nodejs.org/)
- [Caddy 2.6.4+](https://caddyserver.com/)

If you use [`asdf`](https://github.com/asdf-vm/asdf), you can run:

- `asdf plugin-add caddy https://github.com/salasrod/asdf-caddy.git`
- `asdf install`

## Getting Started

ℹ️ Todos are not rendered by default as for some users there can be many! See the
`VUE_APP_RENDER_TODOS` setting in the [Vue variables](#vue-variables) section for
how to enable rendering Todos.

### Preparation

1. Create a [GitLab Personal Access Token (PAT)](https://gitlab.com/-/profile/personal_access_tokens)
called `VUE_APP_GITLAB_DASHBOARD_API_TOKEN` with the `read_api` scope only.

### Run

1. Clone this repository
1. Create a file in the root of your checkout called `.env.local` and ensure it has restricted permissions:

    ```sh
   touch .env.local && chmod 600 .env.local
   ```

1. Open the `.env.local` file previously created earlier and ensure it has the following contents:

    ```plain
    VUE_APP_GITLAB_DASHBOARD_API_TOKEN=<YOUR-PAT-CREATED-EARLIER>
    ```

1. Run `make` which will compile all assets into the `dist/` directory and run the `caddy` webserver.
1. After a few moments, you should be able to open up http://localhost:8080/ in your browser and see
your dashboard. See the `GITLAB_DASHBOARD_CADDY_LISTEN_PORT` setting in the
[Environment variables](#environment-variables) section for adjusting to a different
port to listen on if you already having something listening on port `8080`.

## Configuration

### Vue variables

The following variables are supported when set in your `.env.local` file:

| Variable                             | Values                                        | Description                                                                                                                                            |
|--------------------------------------|-----------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------|
| `VUE_APP_GITLAB_URL`                 | URL (default `https://gitlab.com`)            | A GitLab URL.                                                                                                                                          |
| `VUE_APP_GITLAB_DASHBOARD_API_TOKEN` | Your PAT token.                               | This is required for correct operation.                                                                                                                |
| `VUE_APP_MAX_TODOS`                  | Number of TODOs to render (default `100`)     | Explicitly setting a non-zero value implies `VUE_APP_RENDER_TODOS=true`.<br/>A value greater than 100 will be treated as 100 due to GitLab API limits. |
| `VUE_APP_RENDER_LABELS`              | `true` or `false` (default `true`)            | Render Labels or not.                                                                                                                                  |
| `VUE_APP_RENDER_TODOS`               | `true` or `false` (default `false`)           | Render TODOs or not.                                                                                                                                   |
| `VUE_APP_REFRESH_MINS`               | Minutes, e.g. `5` (default and lowest is `3`) | How frequenty to refresh the page, `0` to disable.                                                                                                     |
| `VUE_APP_CUSTOM_ISSUES`              | JSON Array (default `{}`)                     | Optional JSON Array for adding custom Issue section(s) (e.g. `Deliverables`). See below for example.                                                   |

#### `VUE_APP_CUSTOM_ISSUES` example

The following JSON creates two custom Issue sections:

- Deliverables - Filter Issue(s) that have label(s) `Deliverable` _but_ does not have label(s) `Stretch`.
- Stretch Deliverables - Filter Issue(s) that have label(s) `Deliverable` _and_ `Stretch`.

```json
[
  {
    "name": "Deliverables",
    "labels_inc": [
      "Deliverable"
    ],
    "labels_exc": [
      "Stretch"
    ]
  },
  {
    "name": "Stretch Deliverables",
    "labels_inc": [
      "Deliverable",
      "Stretch"
    ]
  }
]
```

Assigned to the `VUE_APP_CUSTOM_ISSUES` variable:

```
VUE_APP_CUSTOM_ISSUES='[ { "name": "Deliverables", "labels_inc": [ "Deliverable" ], "labels_exc": [ "Stretch" ] }, { "name": "Stretch Deliverables", "labels_inc": [ "Deliverable", "Stretch" ] } ]'
```

### Environment variables

The following environment variables are supported:

| Variable | Values | Description |
| -------- | ----- | ----------- |
| `GITLAB_DASHBOARD_CADDY_LISTEN_PORT` | TCP port, e.g. `1234` (default `8080`) | Local TCP port to listen on. |

## Development

Run `make dev` and within a few moments, you should be able to open up http://localhost:8080/ in
your browser and see your dashboard. In development mode, you can make code changes
and they will be effective almost immediately.

## Contributing

Merge/pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
