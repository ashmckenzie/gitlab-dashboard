.PHONY: prod ensure-credentials build caddy dev install-vue-cli clean-prod clean

GITLAB_DASHBOARD_CADDY_LISTEN_PORT ?= 8080
GITLAB_DASHBOARD_CADDY_LISTEN_ADDR := 127.0.0.1:${GITLAB_DASHBOARD_CADDY_LISTEN_PORT}

prod: ensure-credentials clean-prod build caddy

caddy:
ifeq (, $(shell command -v caddy 2> /dev/null))
  @echo "INFO: caddy is not installed. On macOS you can run 'brew install caddy'"
else
	@echo "INFO: Firing up Caddy at http://${GITLAB_DASHBOARD_CADDY_LISTEN_ADDR}"
	@echo
	@caddy file-server --access-log --listen ${GITLAB_DASHBOARD_CADDY_LISTEN_ADDR} --root dist/
endif

ensure-credentials:
	@test -f .env.local || (echo "ERROR: '.env.local' is missing. Please see the 'Getting Started' section of the README.md" ; false)

build: install-vue-cli
	@npm run build

dev: ensure-credentials install-vue-cli
	@npm run serve

install-vue-cli:
	@npm install vue-cli

clean-prod:
	@rm -rf ./dist/

clean:
	@rm -rf ./node_modules/ ./dist/
