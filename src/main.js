import Vue from 'vue'
import underscore from 'vue-underscore';
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import "@gitlab/ui/dist/index.css";
import App from './App.vue'

Vue.use(BootstrapVue)
Vue.use(underscore);

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
